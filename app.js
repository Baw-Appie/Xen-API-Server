var express = require('express')
var app = express();
var https = require('https')
var bodyParser = require('body-parser');
var fs = require('fs')
var ss = require('./config/server_settings');

app.use(bodyParser.urlencoded({extended: false}));
app.use( require('./libs/logging') );

app.use( function(req, res, next) {
  if(req.query.key && req.query.host){
    if(req.query.key != ss.http.key){
      res.json({ success: false, message: "KEY가 일치하지 않습니다." })
    } else {
      next()
    }
  } else {
    res.json({ success: false, message: "모든 요청에 대한 공통적인 파라미터가 부족합니다."})
  }
})

const { createClient } = require('xen-api')


var no = 0
var xapi = {}
while ( no < ss.xen.length ) {
  let num = no
  var item = ss.xen[no]
  console.log(no+"번 호스트 연결 정의중 ("+item.host+")")
  xapi[num] = createClient({
    url: 'http://'+item.host,
    allowUnauthorized: false,
    auth: {
      user: item.user,
      password: item.pass
    },
    readOnly: false
  })
  xapi[num].connect().then(() => {
    console.log(num+"번 호스트 XenServer와 연결되었습니다. 이제 해당 호스트에 대한 API 요청을 사용할 수 있습니다.")
  })
  no++
}

app.use((req, res, next) => {
  if(xapi[req.query.host] == undefined) {
    res.json({ success: false, message: "정의되지 않은 서버입니다." })
  } else {
    next()
  }
})

app.get('/v1/getFreeRAM', function(req, res){
  xapi[req.query.host].call('host.get_by_uuid', ss.xen[req.query.host].uuid)
  .then(function (host) {
    xapi[req.query.host].call('host.compute_free_memory', host)
    .then(function (freememory) {
      var data = freememory
      xapi[req.query.host].call('VM.get_all')
      .then(function (vms) {
        (async function getFreeRAM() {
          var no = 0
          while ( no <= vms.length ) {
            let num = no
            let vm = vms[num];
            try {
              record = await xapi[req.query.host].call('VM.get_record', vm)
              power = await xapi[req.query.host].call('VM.get_power_state', vm)
              memory = await xapi[req.query.host].call('VM.get_memory_static_max', vm)
            } catch { }
            if(power == "Halted" && record.is_a_template == false){
              data = data-memory
            }
            if(num+1 == vms.length){
              res.json({ success: true, message: "정보를 받아왔습니다.", data: data })
            }
            no++
          }
        })()
      })
    })
  })
  .catch(function (err) {
    res.json({ success: false, message: "서버 내부 오류입니다.", data: { message: err.message } })
  })
})
app.get('/v1/getIP', function(req, res){
  var uuid = req.query.uuid
  if(uuid == undefined){
    res.json({ success: false, message: "UUID는 필수 값 입니다." })
  } else {
    xapi[req.query.host].call('VM.get_by_uuid', uuid)
    .then(function (vm) {
      xapi[req.query.host].call('VM.get_record', vm)
      .then(function (record){
        xapi[req.query.host].call('VM.get_guest_metrics', vm)
        .then(function (metrics) {
          xapi[req.query.host].call('VM_guest_metrics.get_networks', metrics)
          .then(function (network) {
            if(network.toString().indexOf("ip") != -1){
              res.json({ success: false, message: "네트워크 정보를 불러올 수 없습니다." })
            } else {
              res.json({ success: true, message: "네트워크 정보를 받아왔습니다.", data: { ipv4: network } })
            }
          })
        })
      })
    })
    .catch(function (err) {
      res.json({ success: false, message: "서버 내부 오류입니다.", data: { message: err.message } })
    })
  }
})
app.get('/v1/getConsole', function(req, res){
  var uuid = req.query.uuid
  if(uuid == undefined){
    res.json({ success: false, message: "UUID는 필수 값 입니다." })
  } else {
    xapi[req.query.host].call('VM.get_by_uuid', uuid)
    .then(function (vm) {
      xapi[req.query.host].call('VM.get_consoles', vm)
      .then(function (data) {
        xapi[req.query.host].call('console.get_location', data[0])
        .then(function (data2){
          res.json({ success: true, message: "성공했습니다.", data: data2 })
        })
      })
    })
    .catch(function (err) {
      res.json({ success: false, message: "서버 내부 오류입니다.", data: { message: err.message } })
    })
  }
})


app.get('/v1/getRecord', function(req, res){
  var uuid = req.query.uuid
  if(uuid == undefined){
    res.json({ success: false, message: "UUID는 필수 값 입니다." })
  } else {
    xapi[req.query.host].call('VM.get_by_uuid', uuid)
    .then(function (vm) {
      xapi[req.query.host].call('VM.get_record', vm)
      .then(function (record){
        if(record.uuid == uuid){
          res.json({ success: true, message: "레코드 정보를 불러왔습니다.", data: { record: record } })
        }
      })
    })
    .catch(function (err){
      res.json({ success: false, message: "서버 내부 오류입니다.", data: { message: err.message } })
    })
  }
})




app.get('/v1/delete', function(req, res){
  var uuid = req.query.uuid
  if(uuid == undefined){
    res.json({ success: false, message: "UUID는 필수 값 입니다." })
  } else {
    xapi[req.query.host].call('VM.get_by_uuid', uuid)
    .then(function (vm) {
      xapi[req.query.host].call('VM.get_VBDs', vm)
      .then(function (vdbs) {
        var no = 1
        while ( no <= vdbs.length ) {
          let num = no
          xapi[req.query.host].call('VBD.get_record', vdbs[num-1])
          .then(function (vdb){
            if(vdb.type == "Disk"){
              xapi[req.query.host].call('VDI.destroy', vdb.VDI)
            }
          })


          no++
          if(num == vdbs.length){
            xapi[req.query.host].call('VM.destroy', vm)
            .then(function (){
              res.json({ success: true, message: "성공했습니다." })
            })
          }
        }

      })
    })
    .catch(function (err){
      res.json({ success: false, message: "서버 내부 오류입니다.", data: { message: err.message } })
    })
  }
})
app.get('/v1/getPowerState', function(req, res){
  var uuid = req.query.uuid
  if(uuid == undefined){
    res.json({ success: false, message: "UUID는 필수 값 입니다." })
  } else {
    xapi[req.query.host].call('VM.get_by_uuid', uuid)
    .then(function (vm) {
      xapi[req.query.host].call('VM.get_power_state', vm)
      .then(function (record){
        res.json({ success: true, message: "레코드 정보를 불러왔습니다.", data: { record: record } })
      })
    })
    .catch(function (err){
      res.json({ success: false, message: "서버 내부 오류입니다.", data: { message: err.message } })
    })
  }
})

app.get('/v1/Start', function(req, res){
  var uuid = req.query.uuid
  if(uuid == undefined) {
    res.json({ success: false, message: "UUID는 필수 값 입니다."})
  } else {
    xapi[req.query.host].call('VM.get_by_uuid', uuid)
    .then(function (vm) {
      xapi[req.query.host].call('VM.get_record', vm)
      .then(function (record){
        if(record.power_state == "Halted"){
          xapi[req.query.host].call('VM.start', vm, false, true)
          .then(function(){
            res.json({ success: true, message: "VM 시작이 요청되었습니다." })
          })
          .catch(function (err){
            res.json({ success: false, message: "서버 내부 오류입니다.", data: { message: err.message } })
          })
        } else {
          res.json({ success: false, message: "VM 종료 상태가 아닙니다." })
        }
      })
    })
    .catch(function (err){
      res.json({ success: false, message: "서버 내부 오류입니다.", data: { message: err.message } })
    })
  }
})

app.get('/v1/Shutdown', function(req, res){
  var uuid = req.query.uuid
  if(uuid == undefined) {
    res.json({ success: false, message: "UUID는 필수 값 입니다."})
  } else {
    xapi[req.query.host].call('VM.get_by_uuid', uuid)
    .then(function (vm) {
      xapi[req.query.host].call('VM.get_record', vm)
      .then(function (record){
        if(record.power_state == "Running"){
          xapi[req.query.host].call('VM.clean_shutdown', vm)
          .catch(function (err){
            res.json({ success: false, message: "서버 내부 오류입니다.", data: { message: err.message } })
          })
          res.json({ success: true, message: "VM 종료가 요청되었습니다." })
        } else {
          res.json({ success: false, message: "VM이 실행중이 아닙니다."})
        }
      })
    })
    .catch(function (err){
      res.json({ success: false, message: "서버 내부 오류입니다.", data: { message: err.message } })
    })
  }
})

app.get('/v1/HardShutdown', function(req, res){
  var uuid = req.query.uuid
  if(uuid == undefined) {
    res.json({ success: false, message: "UUID는 필수 값 입니다."})
  } else {
    xapi[req.query.host].call('VM.get_by_uuid', uuid)
    .then(function (vm) {
      xapi[req.query.host].call('VM.get_record', vm)
      .then(function (record){
        if(record.power_state == "Running"){
          xapi[req.query.host].call('VM.hard_shutdown', vm)
          .catch(function (err){
            res.json({ success: false, message: "서버 내부 오류입니다.", data: { message: err.message } })
          })
          res.json({ success: true, message: "VM 종료가 요청되었습니다." })
        } else {
          res.json({ success: false, message: "VM이 실행중이 아닙니다."})
        }
      })
    })
    .catch(function (err){
      res.json({ success: false, message: "서버 내부 오류입니다.", data: { message: err.message } })
    })
  }
})

app.get('/v1/Clone', function(req, res){
  var uuid = req.query.uuid
  var name = req.query.name
  if(uuid == undefined || name == undefined) {
    res.json({ success: false, message: "UUID과 name는 필수 값 입니다."})
  } else {
    xapi[req.query.host].call('VM.get_by_uuid', uuid)
    .then(function (vm) {
      xapi[req.query.host].call('VM.get_record', vm)
      .then(function (record){
        xapi[req.query.host].call('VM.clone', vm, name)
        .then(function (data){
          xapi[req.query.host].call('VM.get_record', data)
          .then(function (record2){
            res.json({ success: true, message: "성공했습니다.", data: record2 })
          })
        })
        .catch(function (err){
          res.json({ success: false, message: "서버 내부 오류입니다.", data: { message: err.message } })
        })
      })
    })
    .catch(function (err){
      res.json({ success: false, message: "서버 내부 오류입니다.", data: { message: err.message } })
    })
  }
})
app.get('/v1/setvCore', function(req, res){
  var uuid = req.query.uuid
  var core = req.query.core
  if(uuid == undefined || core == undefined) {
    res.json({ success: false, message: "UUID과 core는 필수 값 입니다."})
  } else {
    xapi[req.query.host].call('VM.get_by_uuid', uuid)
    .then(function (vm) {
      xapi[req.query.host].call('VM.get_record', vm)
      .then(function (record){
        xapi[req.query.host].call('VM.set_VCPUs_at_startup', vm, core)
        .then(function (data){
          res.json({ success: true, message: "성공했습니다." })
        })
        .catch(function (err){
          res.json({ success: false, message: "서버 내부 오류입니다.", data: { message: err.message } })
        })
      })
    })
    .catch(function (err){
      res.json({ success: false, message: "서버 내부 오류입니다.", data: { message: err.message } })
    })
  }
})
app.get('/v1/setRam', function(req, res){
  var uuid = req.query.uuid
  var ram = req.query.ram
  if(uuid == undefined || ram == undefined) {
    res.json({ success: false, message: "UUID과 ram은 필수 값 입니다."})
  } else {
    xapi[req.query.host].call('VM.get_by_uuid', uuid)
    .then(function (vm) {
      xapi[req.query.host].call('VM.get_record', vm)
      .then(function (record){
        xapi[req.query.host].call('VM.set_memory_limits', vm, ram, ram, ram, ram)
        .then(function (data){
          res.json({ success: true, message: "성공했습니다." })
        })
        .catch(function (err){
          res.json({ success: false, message: "서버 내부 오류입니다.", data: { message: err.message } })
        })
      })
    })
    .catch(function (err){
      res.json({ success: false, message: "서버 내부 오류입니다.", data: { message: err.message } })
    })
  }
})
app.get('/v1/getRam', function(req, res){
  var uuid = req.query.uuid
  var ram = req.query.ram
  if(uuid == undefined || ram == undefined) {
    res.json({ success: false, message: "UUID과 ram은 필수 값 입니다."})
  } else {
    xapi[req.query.host].call('VM.get_by_uuid', uuid)
    .then(function (vm) {
      xapi[req.query.host].call('VM.get_record', vm)
      .then(function (record){
        xapi[req.query.host].call('VM.get_memory_static_max', vm)
        .then(function (data){
          res.json({ success: true, message: "성공했습니다.", data: data})
        })
        .catch(function (err){
          res.json({ success: false, message: "서버 내부 오류입니다.", data: { message: err.message } })
        })
      })
    })
    .catch(function (err){
      res.json({ success: false, message: "서버 내부 오류입니다.", data: { message: err.message } })
    })
  }
})
app.get('/v1/getvCore', function(req, res){
  var uuid = req.query.uuid
  var ram = req.query.ram
  if(uuid == undefined || ram == undefined) {
    res.json({ success: false, message: "UUID과 ram은 필수 값 입니다."})
  } else {
    xapi[req.query.host].call('VM.get_by_uuid', uuid)
    .then(function (vm) {
      xapi[req.query.host].call('VM.get_record', vm)
      .then(function (record){
        xapi[req.query.host].call('VM.get_VCPUs_at_startup', vm)
        .then(function (data){
          res.json({ success: true, message: "성공했습니다.", data: data})
        })
        .catch(function (err){
          res.json({ success: false, message: "서버 내부 오류입니다.", data: { message: err.message } })
        })
      })
    })
    .catch(function (err){
      res.json({ success: false, message: "서버 내부 오류입니다.", data: { message: err.message } })
    })
  }
})



app.all('*', function(req, res){
  res.json({ success: false, message: "알 수 없는 요청입니다."})
})
var ssloptions = {
   ca: fs.readFileSync('./config/ssl/ca.pem', 'utf8'),
   key: fs.readFileSync('./config/ssl/key.pem', 'utf8'),
   cert: fs.readFileSync('./config/ssl/cert.pem', 'utf8')
}
var https_server = https.createServer(ssloptions, app);

https_server.listen(ss.http.https_port, '0.0.0.0', function(){
  console.log("[Xen-API Server] HTTPS server listening on port " + https_server.address().port);
});
