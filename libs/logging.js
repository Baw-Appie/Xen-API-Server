module.exports = function(req, res, next) {
  var time = new Date().toLocaleTimeString();
    console.log('['+time+'] [Access] '+req.ip+': '+req.path+' ('+req.method+')')
  next();
};
